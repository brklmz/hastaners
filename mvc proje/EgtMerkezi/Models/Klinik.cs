//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EgtMerkezi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Klinik
    {
        public Klinik()
        {
            this.Rondevu = new HashSet<Rondevu>();
        }
    
        public int KlinikId { get; set; }
        public string Ad { get; set; }
    
        public virtual ICollection<Rondevu> Rondevu { get; set; }
    }
}
