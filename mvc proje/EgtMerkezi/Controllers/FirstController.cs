﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EgtMerkezi.Models;
namespace EgtMerkezi.Controllers
{
    public class FirstController : Controller
    {
        private RandevuEntities1 db = new RandevuEntities1();
        // GET: First
        public ActionResult Index()
        {
            ViewBag.ILID = new SelectList(db.IL, "ILID", "ILADI");
            ViewBag.ILCEID = new SelectList(db.ILCE,"ILCEID","ILCEAD");
            ViewBag.DOKTORID = new SelectList(db.Doctor, "DOKTORID", "AD");
          
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}