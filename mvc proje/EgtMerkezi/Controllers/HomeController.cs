﻿using EgtMerkezi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace EgtMerkezi.Controllers
{
    public class HomeController : Controller
    {
        RandevuEntities1 ent = new RandevuEntities1();
        public ActionResult Index()
        {
        
            AnaSayfaDTO obj = new AnaSayfaDTO();
            obj.slider = ent.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
            obj.duyuru = ent.Duyurular.OrderByDescending(x => x.ID).Take(3).ToList();
            obj.gorus = ent.HastaGorus.OrderByDescending(x => x.ID).Take(3).ToList();
            return View("Index", obj);
        }
        public ActionResult UyeGiris()
        {
            if(Session["UyeId"] == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index","Home");
            }
        }
        [HttpPost]
        public ActionResult UyeGiris(Uyeler uye)
        {
            if(Session["UyeId"] == null)
            {
                if(ModelState.IsValid)
                {
                    var uyeler = ent.Uyeler.SingleOrDefault(p => p.UyeTc == uye.UyeTc && p.UyeSifre == uye.UyeSifre);
                    if(uyeler==null)
                    {
                        return View(uye);
                    }
                    else
                    {
                        Session["UyeId"] = uyeler.UyeId;
                        Session["UyeRol"] = uyeler.UyeRol;
                        Session["UyeAdi"] = uyeler.UyeAdi;
                        if(Session["UyeRol"].ToString() == "Admin")
                        {
                            return RedirectToAction("Slider","Admin");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
                return View(uye);
            }
            else
            {
                return RedirectToAction("Index","Home");
            }
            
        }
        public ActionResult UyeKayit()
        {
            if(Session["UyeId"] == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index","Home");
            }
        }
        [HttpPost]
        public ActionResult UyeKayit(Uyeler uye)
        {
            if(Session["UyeId"] == null)
            {
                if(ModelState.IsValid)
                {
                    uye.UyeRol = "Kullanıcı";
                    ent.Uyeler.Add(uye);
                    ent.SaveChanges();

                    Session["UyeId"] = uye.UyeId;
                    Session["UyeRol"] = uye.UyeRol;
                    Session["UyeAdi"] = uye.UyeAdi;

                    return RedirectToAction("Index","Home");
                }
                return View(uye);
            }
            return View(uye);
        }
        public ActionResult Cikis()
        {
            Session["UyeRol"] = null;
            Session["UyeId"] = null;
            Session["UyeAdi"] = null;
            return RedirectToAction("Index","Home");
         }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult GoogleSearch()
        {
            return View();
        }
        public ActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contact(Oneri form)
        {
            try
            {
                Oneri _iletisimform = new Oneri();
                _iletisimform.AdSoyad = form.AdSoyad;
                _iletisimform.Telefon = form.Telefon;
                _iletisimform.Eposta = form.Eposta;
                _iletisimform.Mesaj = form.Mesaj;
                _iletisimform.Tarih = DateTime.Now;
                ent.Oneri.Add(_iletisimform);
                ent.SaveChanges();
                TempData["Mesaj"] = "Form Başarıyla gönderilmiştir.";
                return Index();
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        
        public ActionResult Question()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        public ActionResult Randevu()
        {
            if(Session["UyeId"] == null)
            {
                return RedirectToAction("UyeGiris","Home");
            }
            else
            {

                ViewBag.ILID = new SelectList(ent.IL, "ILID", "ILADI");
                ViewBag.ILCEID = new SelectList(ent.ILCE, "ILCEID", "ILCEAD");
                ViewBag.DOKTORID = new SelectList(ent.Doctor, "DOKTORID", "AD");
                ViewBag.HastaneId = new SelectList(ent.Hospital, "HastaneId", "AD");
                ViewBag.KlinikId = new SelectList(ent.Klinik, "KlinikId", "AD");


                return View();
            }                                        
           }
        protected override void Dispose(bool disposing)
        {
            ent.Dispose();
            base.Dispose(disposing);
        }
        

        public ActionResult RandevuGoruntule()
        {
            if(Session["UyeId"] == null)
            {
                return RedirectToAction("UyeGiris","Home");
            }
            else
            {
                if (Session["UyeRol"].ToString() == "Kullanıcı")
                {
                    ViewData["randevu"] = ent.Rondevu.ToList().Where(p => p.UyeId == Convert.ToInt32(Session["UyeId"]));
                    ViewData["iller"] = ent.IL;
                    ViewData["ilceler"] = ent.ILCE;
                    ViewData["hastaneler"] = ent.Hospital;
                    ViewData["doktorlar"] = ent.Doctor;
                    ViewData["poliklinikler"] = ent.Klinik;

                    return View();
                }
                else if (Session["UyeRol"].ToString() == "Admin")
                {
                    ViewData["randevu"] = ent.Rondevu.ToList();
                    ViewData["iller"] = ent.IL;
                    ViewData["ilceler"] = ent.ILCE;
                    ViewData["hastaneler"] = ent.Hospital;
                    ViewData["doktorlar"] = ent.Doctor;
                    ViewData["poliklinikler"] = ent.Klinik;
                    ViewData["Uyeler"] = ent.Uyeler;
                    return View();

                }
                else
                {
                    return RedirectToAction("UyeGiris", "Home");
                }
            }
            
        }
       
        [HttpPost]
        public ActionResult Randevu(Rondevu rnd)
        {
            try
            {
                Rondevu _randevu = new Rondevu();
                _randevu.RandevuTarihSaat = rnd.RandevuTarihSaat;
                _randevu.DOKTORID = rnd.DOKTORID;
                _randevu.ILID = rnd.ILID;
                _randevu.ILCEID = rnd.ILCEID;
                _randevu.KlinikId = rnd.KlinikId;
                _randevu.HastaneId = rnd.HastaneId;
                _randevu.UyeId = Convert.ToInt32(Session["UyeId"]);
                ent.Rondevu.Add(_randevu);
                ent.SaveChanges();
                return RedirectToAction("Index","Home");
            }
            catch
            {
                var ilceler = ent.ILCE.Where(p => p.ILID == rnd.ILID);
                ViewBag.ILID = new SelectList(ent.IL, "ILID", "ILADI");
                ViewBag.ILCEID = new SelectList(ilceler, "ILCEID", "ILCEAD");
                ViewBag.DOKTORID = new SelectList(ent.Doctor, "DOKTORID", "AD");
                ViewBag.HastaneId = new SelectList(ent.Hospital, "HastaneId", "AD");
                ViewBag.KlinikId = new SelectList(ent.Klinik, "KlinikId", "AD");
                return View(rnd);
            }
        }

      
    }
    

    public class AnaSayfaDTO
    {
        public List<Slider> slider { get; set; }
        public List<HastaGorus> gorus { get; set; }
        public List<Duyurular> duyuru { get; set; }
        
    }
}