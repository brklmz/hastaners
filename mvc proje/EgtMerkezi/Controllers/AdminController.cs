﻿using EgtMerkezi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EgtMerkez.Controllers
{
    public class AdminController : Controller
    {
        RandevuEntities1 ent = new RandevuEntities1();

        #region // Slider

        public ActionResult Slider()
        {
            try
            {
                if (Session["UyeRol"].ToString() == "Admin")
                {
                    var slider = ent.Slider.ToList();
                    return View(slider);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            catch
            {
                return RedirectToAction("Index","Home");
            }
            
        }
        public ActionResult SlideEkle()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult SlideDuzenle(int SlideID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var _slideDuzenle = ent.Slider.Where(x => x.ID == SlideID).FirstOrDefault();
                return View(_slideDuzenle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult SlideSil(int SlideID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                try
                {
                    ent.Slider.Remove(ent.Slider.First(d => d.ID == SlideID));
                    ent.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        public ActionResult SlideEkle(Slider s, HttpPostedFileBase file)
        {
            try
            {
                Slider _slide = new Slider();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.SliderFoto = memoryStream.ToArray();
                }
                _slide.SliderText = s.SliderText;
                _slide.BaslangicTarih = s.BaslangicTarih;
                _slide.BitisTarih = s.BitisTarih;
                ent.Slider.Add(_slide);
                ent.SaveChanges();
                return RedirectToAction("Slider", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult SlideDuzenle(Slider slide, HttpPostedFileBase file)
        {
            try
            {
                var _slideDuzenle = ent.Slider.Where(x => x.ID == slide.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slideDuzenle.SliderFoto = memoryStream.ToArray();
                }
                _slideDuzenle.SliderText = slide.SliderText;
                _slideDuzenle.BaslangicTarih = slide.BaslangicTarih;
                _slideDuzenle.BitisTarih = slide.BitisTarih;
                ent.SaveChanges();
                return RedirectToAction("Slider", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion       
        #region // Doktor

        public ActionResult Doktor()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var doktor = ent.Doctor.ToList();
                return View(doktor);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DoktorEkle()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DoktorDuzenle(int DoktorId)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var _doktorDuzenle = ent.Doctor.Where(x => x.DOKTORID == DoktorId).FirstOrDefault();
                return View(_doktorDuzenle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DoktorSil(int DoktorId)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                try
                {
                    ent.Doctor.Remove(ent.Doctor.First(d => d.DOKTORID == DoktorId));
                    ent.SaveChanges();
                    return RedirectToAction("Doktor", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        public ActionResult DoktorEkle(Doctor dktr, HttpPostedFileBase file)
        {
            try
            {
                Doctor _doktor = new Doctor();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _doktor.Foto = memoryStream.ToArray();
                }
                _doktor.Ad = dktr.Ad;
                _doktor.Soyad = dktr.Soyad;
                _doktor.Telefon = dktr.Telefon;
                _doktor.Eposta = dktr.Eposta;
                _doktor.Ozgecmis = dktr.Ozgecmis;
                ent.Doctor.Add(_doktor);
                ent.SaveChanges();
                return RedirectToAction("Doktor", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult DoktorDuzenle(Doctor dktrr, HttpPostedFileBase file)
        {
            try
            {
                var _doktorDuzenle = ent.Doctor.Where(x => x.DOKTORID == dktrr.DOKTORID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _doktorDuzenle.Foto = memoryStream.ToArray();
                }
                _doktorDuzenle.Ad = dktrr.Ad;
                _doktorDuzenle.Soyad = dktrr.Soyad;
                _doktorDuzenle.Telefon = dktrr.Telefon;
                _doktorDuzenle.Eposta = dktrr.Eposta;
                _doktorDuzenle.Ozgecmis = dktrr.Ozgecmis;
                ent.SaveChanges();
                return RedirectToAction("Doktor", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion
        #region // Hastane

        public ActionResult Hastane()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var hastane = ent.Hospital.ToList();
                return View(hastane);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult HastaneEkle()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult HastaneDuzenle(int HastaneID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var _hastaneDuzenle = ent.Hospital.Where(x => x.HastaneId == HastaneID).FirstOrDefault();
                return View(_hastaneDuzenle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult HastaneSil(int HastaneID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                try
                {
                    ent.Hospital.Remove(ent.Hospital.First(d => d.HastaneId == HastaneID));
                    ent.SaveChanges();
                    return RedirectToAction("Hastane", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        public ActionResult HastaneEkle(Hospital hstane, HttpPostedFileBase file)
        {
            try
            {
                Hospital _hospital = new Hospital();
               
                    
                
                _hospital.Ad = hstane.Ad;
                 ent.Hospital.Add(_hospital);
                ent.SaveChanges();
                return RedirectToAction("Hastane", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult HastaneDuzenle(Hospital hstane, HttpPostedFileBase file)
        {
            try
            {
                var _hastaneDuzenle = ent.Hospital.Where(x => x.HastaneId == hstane.HastaneId).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    
                }
                _hastaneDuzenle.Ad = hstane.Ad;
                
                ent.SaveChanges();
                return RedirectToAction("Hastane", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion
       
        #region // Poliklinik

        public ActionResult Poliklinik()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var klinik = ent.Klinik.ToList();
                return View(klinik);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult PoliklinikEkle()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult PoliklinikDuzenle(int KlinikID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var _poliklinikDuzenle = ent.Klinik.Where(x => x.KlinikId == KlinikID).FirstOrDefault();
                return View(_poliklinikDuzenle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult PoliklinikSil(int KlinikID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                try
                {
                    ent.Klinik.Remove(ent.Klinik.First(d => d.KlinikId == KlinikID));
                    ent.SaveChanges();
                    return RedirectToAction("Poliklinik", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        public ActionResult PoliklinikEkle(Klinik klnk, HttpPostedFileBase file)
        {
            try
            {
                Klinik _klinik = new Klinik();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    
                }
                _klinik.Ad = klnk.Ad;
                ent.Klinik.Add(_klinik);
                ent.SaveChanges();
                return RedirectToAction("Poliklinik", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult PoliklinikDuzenle(Klinik klnk, HttpPostedFileBase file)
        {
            try
            {
                var _klinikDuzenle = ent.Klinik.Where(x => x.KlinikId == klnk.KlinikId).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    
                }
                _klinikDuzenle.Ad = klnk.Ad;
                
                ent.SaveChanges();
                return RedirectToAction("Poliklinik", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

        #region // Duyuru

        public ActionResult Duyurular()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var duyurular = ent.Duyurular.ToList();
                return View(duyurular);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DuyuruEkle()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DuyuruDuzenle(int DuyuruID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var _duyuruDuzenle = ent.Duyurular.Where(x => x.ID == DuyuruID).FirstOrDefault();
                return View(_duyuruDuzenle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DuyuruSil(int DuyuruID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                try
                {
                    ent.Duyurular.Remove(ent.Duyurular.First(d => d.ID == DuyuruID));
                    ent.SaveChanges();
                    return RedirectToAction("Duyurular", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        public ActionResult DuyuruEkle(Duyurular d)
        {
            try
            {
                Duyurular _duyuru = new Duyurular();
                _duyuru.DuyuruBaslik = d.DuyuruBaslik;
                _duyuru.Duyuru = d.Duyuru;
                _duyuru.Tarih = DateTime.Now;
                ent.Duyurular.Add(_duyuru);
                ent.SaveChanges();
                return RedirectToAction("Duyurular", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult DuyuruDuzenle(Duyurular dyr)
        {
            try
            {
                var _duyuruDuzenle = ent.Duyurular.Where(x => x.ID == dyr.ID).FirstOrDefault();
                _duyuruDuzenle.DuyuruBaslik = dyr.DuyuruBaslik;
                _duyuruDuzenle.Duyuru = dyr.Duyuru;
                ent.SaveChanges();
                return RedirectToAction("Duyurular", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion

        #region Hasta Görüşleri

        public ActionResult HastaGorusleri()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var hastagorus = ent.HastaGorus.ToList();
                return View(hastagorus);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult HastaGorusEkle()
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult HastaGorusDuzenle(int GorusID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                var _hastagorusDuzenle = ent.HastaGorus.Where(x => x.ID == GorusID).FirstOrDefault();
                return View(_hastagorusDuzenle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult HastaGorusSil(int GorusID)
        {
            if (Session["UyeRol"].ToString() == "Admin")
            {
                try
                {
                    ent.HastaGorus.Remove(ent.HastaGorus.First(d => d.ID == GorusID));
                    ent.SaveChanges();
                    return RedirectToAction("HastaGorusleri", "Admin");
                }
                catch (Exception ex)
                {
                    throw new Exception("Silerken hata oluştu", ex.InnerException);
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        public ActionResult HastaGorusEkle(HastaGorus hst, HttpPostedFileBase file)
        {
            try
            {
                HastaGorus _hastagorus = new HastaGorus();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _hastagorus.Foto = memoryStream.ToArray();
                }
                _hastagorus.AdSoyad = hst.AdSoyad;
                _hastagorus.Gorus = hst.Gorus;
                _hastagorus.Tarih = DateTime.Now;
                ent.HastaGorus.Add(_hastagorus);
                ent.SaveChanges();
                return RedirectToAction("HastaGorusleri", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }
        [HttpPost]
        public ActionResult HastaGorusDuzenle(HastaGorus hasta, HttpPostedFileBase file)
        {
            try
            {
                var _hastagorusDuzenle = ent.HastaGorus.Where(x => x.ID == hasta.ID).FirstOrDefault();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _hastagorusDuzenle.Foto = memoryStream.ToArray();
                }
                _hastagorusDuzenle.AdSoyad = hasta.AdSoyad;
                _hastagorusDuzenle.Gorus = hasta.Gorus;
                ent.SaveChanges();
                return RedirectToAction("HastaGorusleri", "Admin");
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }

        #endregion
    }

}
